public class Types{
	public static void main(String[] args) {
		final int student_record_number = 0X38412;	
		System.out.println("Номер зачетной книжки: " + student_record_number);
		final long phone_number = 89994054175L;
		System.out.println("Номер мобильного телефона: " + phone_number);
		final byte two_last_digits_phone_number = 0b1001011;
		System.out.println("Последние 2 цифры мобильного телефона: " + two_last_digits_phone_number);
		final short four_last_digits_phone_number = 010117;
		System.out.println("Последние 4 цифры мобильного телефона: " + four_last_digits_phone_number);
		final byte transformation_student_number = ((19 - 1) % 26) - 1;
		System.out.println("Измененный порядковый номер студента: " + transformation_student_number);
		char res_abc = transformation_student_number + 64;
		System.out.println("Буква английского алфавита, номер которой соответствует изменнному порядкову номеру студента: " + res_abc);	
	}
}